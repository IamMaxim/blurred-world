To install this theme:
- install Kvantum Manager
- download this theme to some folder
- launch Kvantum Manager
- in the section "Install/Update theme" specify path to the folder and click "Install this theme"
- in the section "Change/Delete theme" select newly installed "Blurred-world" theme
- Done. Enjoy!